from datetime import datetime
from datetime import timedelta


class Stage():
    """Kind of like a timedelta."""

    def __init__(self, name, start, duration):
        self.name = name
        self.start = start
        self.duration = duration
        self.end = self.start + timedelta(minutes=duration)

    @property
    def end_plus_one(self):
        return self.end + timedelta(seconds=1)

    @property
    def started(self):
        return datetime.now().replace(microsecond=0) > self.start

    @property
    def ended(self):
        return datetime.now().replace(microsecond=0) > self.end

    @property
    def running(self):
        return self.started and not self.ended

    @property
    def remaining(self):
        remaining = str(self.end - datetime.now().replace(microsecond=0))
        return ':'.join([i for i in remaining.split(':') if int(i)])


class Pomodoro(Stage):
    """TODO."""

    def __init__(self, tasks=4, task_len=25, short_break_len=5,
                 long_break_len=15, start=None, **kwargs):
        """TODO."""
        self.stages = []
        self.tasks = tasks
        self.task_len = task_len
        self.short_break_len = short_break_len
        self.long_break_len = long_break_len
        self.start = start or datetime.now().replace(microsecond=0)
        self._append_stages()

    def _append_stages(self):
        # Append tasks and short breaks between.
        for idx, task in enumerate(range(self.tasks)):
            # Create new stage from current task.
            try:
                stage_start = self.end_plus_one
            except IndexError:
                stage_start = self.start
            self.stages.append(Stage('task', stage_start, self.task_len))
            if idx + 1 < self.tasks:
                # Append a short break.
                self.stages.append(Stage('short break', self.end_plus_one,
                                         self.short_break_len))
        # Append the long break.
        self.stages.append(Stage('long break', self.end_plus_one,
                                 self.long_break_len))

    def from_string(self, string):
        """Create self from a CSV string."""
        values = string.split(',')
        self.tasks = int(values[0])
        self.task_len = int(values[1])
        self.short_break_len = int(values[2])
        self.long_break_len = int(values[3])
        # Example format: 2019-01-18 23:39:15.211897
        self.start = datetime.strptime(values[4], '%Y-%m-%d %H:%M:%S.%f')
        self._append_stages()

    def from_file(self):
        """Create self from a CSV string."""
        with open('~/.pomodoro') as f:
            for line in f:
                pass
        self.from_string(line)

    @property
    def duration(self):
        """Return pomodoro length in minutes."""
        return sum([stage.duration for stage in self.stages])

    @property
    def end(self):
        """The end time of the final stage."""
        return self.stages[-1].end

    @property
    def current_stage(self):
        """todo"""
        for stage in self.stages:
            if stage.running:
                return stage

    @property
    def settings(self):
        """Return self as a dict."""
        return {
            'tasks': self.tasks,
            'task_len': self.task_len,
            'short_break_len': self.short_break_len,
            'long_break_len': self.long_break_len,
            'start': self.start,
            'end': self.end,
        }

    def __str__(self):
        """Return self as a CSV string.

        '4,25,5,15,2019-01-18T21:06:49,2019-03-28T21:06:49'
        """
        return ','.join(str(i) for i in [
            self.tasks,
            self.task_len,
            self.short_break_len,
            self.long_break_len,
            self.start.isoformat(sep=' '),
            self.end.isoformat(sep=' '),
        ])
