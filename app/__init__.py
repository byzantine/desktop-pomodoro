from flask import Flask
from werkzeug.contrib.fixers import ProxyFix


app = Flask(__name__)
app.config.from_object('app.config')
app.wsgi_app = ProxyFix(app.wsgi_app)

# import statement at end to avoid circular reference; views module imports app
from app import views
