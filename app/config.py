"""TODO."""

APP_NAME = 'POMODORO'
"""
Generate a secret key by running the following:
>>> import os
>>> os.urandom(24)
"""
SECRET_KEY = b"foo"

DEBUG = True

WTF_CSRF_ENABLED = True
