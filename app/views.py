from flask import render_template
from flask import Response
from flask import session

from app import app
from app import pomodoro


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/start', methods=['POST'])
def start():
    # Start the timer if it isn't already started.
    session['p'] = pomodoro.Pomodoro().settings
    response = Response('')
    response.headers['X-IC-ResumePolling'] = 'true'
    return response


@app.route('/stop', methods=['POST'])
def stop():
    response = Response('')
    response.headers['X-IC-CancelPolling'] = 'true'
    return response


@app.route('/remaining')
def remaining():
    p = pomodoro.Pomodoro(**session['p'])
    current_stage = p.current_stage
    return '{} {}'.format(current_stage.name, current_stage.remaining)
