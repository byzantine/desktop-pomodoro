import logging
import time
from threading import Thread
from threading import Lock

import webview

from app import pomodoro
from run import run_server


server_lock = Lock()

logger = logging.getLogger(__name__)


def url_ok(url, port):
    """Return True if given URL reachable, otherwise False.

    Uses httplib on Python 2.
    """
    try:
        from http.client import HTTPConnection
    except ImportError:
        from httplib import HTTPConnection

    try:
        conn = HTTPConnection(url, port)
        conn.request("GET", "/")
        r = conn.getresponse()
        return r.status == 200
    except BaseException:
        logger.exception("Server not started")
    return False


if __name__ == '__main__':
    logger.debug('Starting server')
    t = Thread(target=run_server)
    t.daemon = True
    t.start()

    logger.debug('Checking server')
    while not url_ok('127.0.0.1', 23948):
        time.sleep(1)
    logger.debug('Server started')

    webview.create_window(
        'My first application',
        'http://127.0.0.1:23948',
        width=200,
        height=300,
        min_size=(240, 480),
    )
