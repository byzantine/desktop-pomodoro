#!venv/bin/python
from app import app


def run_server():
    """Run Flask server."""
    app.run(
        host='127.0.0.1',
        port=23948,
        threaded=True,
        # use_reloader=False,
    )


if __name__ == '__main__':
    run_server()
